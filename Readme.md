Cal Poly SLO provides a tool known as PASS: Plan a Student Schedule, which
allows students to browse classes and find sections which fit together. This
project parses saved HTML from PASS to generate an ICS calendar file to make
adding classes to your calendar easier.

## Basic use
1. Save the "Planned Schedule" page from PASS
2. Save the "View Schedule on Map" page from PASS
3. Run something like 
`convert_poly_planner --map map.html --schedule main.html --begin 14-Sep-2017 --end 8-Dec-2017 > schedule.ics`
with the dates changed for the current quarter's start and end dates.

## Getting Started
1. Clone this repo
2. (If you don't already have `bundler` installed, run `gem install bundler`)
3. `cd` into this repo directory
4. Run `bundle install`
5. Run `bin/convert_poly_planner --help` and follow directions. For example, if y
